import Badge from 'react-bootstrap/Badge';
import ListGroup from 'react-bootstrap/ListGroup';

const ListComponent = ({ items }) => {
    console.log('dsf', items);
    return (
        <ListGroup as="ol" numbered>
            {items && items.map((item) => {
                return(
                    <ListGroup.Item
                        as="li"
                        className="d-flex justify-content-between align-items-start"
                    >
                    <div className="ms-2 me-auto">
                    <div className="fw-bold">{item.username}</div>
                    {item._id}
                    </div>
                    <Badge bg="primary" pill>
                    14
                    </Badge>
                    </ListGroup.Item>
                )
            })}
        </ListGroup>
    );
}

export default ListComponent;