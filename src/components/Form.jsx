import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState } from 'react';

function BasicExample() {
    const [emailAddrs, setEmailAddrss] = useState('');
     const elements = [
        { label: 'Email Address', type: 'email' },
        { label: 'First Name', type: 'text' },
        { label: 'Password', type: 'password' },
    ];

    return (
        <Form>
        {elements.map((elem) => {
            return (
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>{elem.label}</Form.Label>
                    <Form.Control type={elem.type} placeholder="Enter email" />
                </Form.Group>
            );
        })}
        <Button variant="primary" type="submit">
            Submit
        </Button>
        <h3>Hi {emailAddrs}</h3>
        <h3>Your firstname is {emailAddrs}</h3>
        <h3>Your password is {emailAddrs}</h3>
        </Form>
    );
}

export default BasicExample;