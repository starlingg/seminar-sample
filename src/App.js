import NavBar from './components/Navbar';
import { Routes, Route } from 'react-router-dom';
import Registration from './pages/Registration';
import LoginPage from './pages/Login';
import HomePage from './pages/Homepage';
import 'bootstrap/dist/css/bootstrap.min.css';

export function App() {
  return (
    <>
      <NavBar />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/registration" element={<Registration />} />
      </Routes>
    </>
  )
}

export default App;