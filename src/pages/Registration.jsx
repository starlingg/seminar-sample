import { useState, useEffect } from 'react';
import Form from '../components/Form';

const RegistrationPage = () => {
    return(
        <div style={{ width: 600, margin: 'auto', marginTop: 40 }}>
            <h3>Registration</h3>
            <Form />
        </div>
    )
};

export default RegistrationPage;