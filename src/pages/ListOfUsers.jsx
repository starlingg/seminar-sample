import React, { useState, useEffect } from 'react';
import axios from "axios";
import List from '../components/List';

const ApiURL = `http://localhost:5000/users`;

const ListOfUsers = () => {
    const [users, setUsers] = useState(null);

    useEffect(() => {
        axios.get(ApiURL).then((response) => {
            setUsers(response?.data);
        });
      }, []);
    
    if (!users) return null;
    
    return (
        <List items={users} />
    )
};

export default ListOfUsers;