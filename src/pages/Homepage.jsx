import React, { useEffect, useState } from 'react';
import axios from 'axios';

const HomePage = () => {
    const [userName, setUsername] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:5000/users')
        .then(response => {
            console.log(response.data)
            setUsername(response.data);
        })
    }, []);

    return (
        <h1>{userName.map((uname) => uname.username)}</h1>
    )
};

export default HomePage;